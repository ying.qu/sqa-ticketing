package de.rwth.swc.sqa.api;

import de.rwth.swc.sqa.model.DiscountCard;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.time.Clock;
import java.time.Instant;
import java.util.List;

@Controller
public class CustomerController implements CustomersApi{

    private final Clock clock;

    CustomerController(Clock clock){
        this.clock = clock;
    }

    @Override
    public ResponseEntity<List<DiscountCard>> getCustomerDiscountCards(Long customerId) {
        Instant instant = Instant.now(clock);
        return CustomersApi.super.getCustomerDiscountCards(customerId);
    }
}
